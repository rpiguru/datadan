import pandas as pd


DEFAULT_COLUMN_MAP = {
    'Standardized Client Name': 'Customer Name',
    'Net Amount': 'Revenue',
    'Period ID': 'Month',
    'Customer ID (Segment 2)': 'Customer ID',
    'P&L Line Item': 'Line Item',
    'P&L Category': 'Category',
    'Mode with Eliminations': 'Other',
}

COLUMN_SIZE_HINTS = {
    'Line Item': 2.3,
    'Category': 1.2,
    'Customer Name': 1.5,
    'Other': 1.4
}


def assign_segment(x):
    """
    function for assigning letter to segment to company based off of revenue in a year
    :param x:
    :return:
    """
    if x > 25000000:
        return "G"
    elif x > 10000000:
        return "F"
    elif x > 1000000:
        return "E"
    elif x > 500000:
        return "D"
    elif x > 100000:
        return "C"
    elif x > 50000:
        return "B"
    else:
        return "A"


def read_raw_data(file_path):
    xls = pd.ExcelFile(file_path)
    data = {}
    for sheet in xls.sheet_names:
        sh_data = pd.read_excel(file_path, sheet_name=sheet)
        if "Unnamed: 0" in sh_data.columns:
            sh_data.drop("Unnamed: 0", 1, inplace=True)
        data[sheet] = sh_data
    return data


def get_revenue_pivot(data, filter_data=None):
    return pd.DataFrame(data.groupby(['Customer Name', 'Year'])['Revenue'].sum().reset_index()).pivot(
        index='Customer Name', columns='Year', values='Revenue').reset_index()


def filter_data_by_columns(data, filter_data):
    for column, value in filter_data.items():
        data = data[data[column] == value]
    return data


def get_net_revenue_pivot(data, columns):
    # filter gross revenue to net revenue
    data_filtered = data.copy()
    if 'Category' in columns:
        data_filtered = data_filtered[data_filtered['Category'] == 'Net Revenue']
    if 'Other' in columns:
        data_filtered = data_filtered[data_filtered['Other'] != 'Brokerage - Eliminate']
        data_filtered = data_filtered[data_filtered['Other'] != 'Sourcing - Eliminate']
    if 'Line Item' in columns:
        data_filtered = data_filtered[data_filtered['Line Item'] != 'LTL Resale Margin (CSS)']
    # Create Net Revenue table
    net_revenue = pd.DataFrame(data_filtered.groupby(['Customer Name', 'Year'])['Revenue'].sum().reset_index())
    return net_revenue.rename(columns={"Revenue": "Net Revenue"}).pivot(
        index='Customer Name', columns='Year', values='Net Revenue').reset_index()


def get_revenue_margin_pivot(data, columns=None):
    revenue_pivot = get_revenue_pivot(data)
    net_revenue_pivot = get_net_revenue_pivot(data, columns)

    revenue = pd.DataFrame(data.groupby(['Customer Name', 'Year'])['Revenue'].sum().reset_index())
    revenue_clean = revenue[revenue['Revenue'] != 0]

    # assign time cohort to each company
    cohort_assigner = revenue_clean.groupby(by='Customer Name')['Year'].min().reset_index()
    cohort_assigner = cohort_assigner.rename(columns={"Year": "Cohort"})

    # fill na's with 0's
    gross_revenue_filled = revenue_pivot.fillna(0).melt(id_vars='Customer Name', var_name='Year', value_name='Revenue')
    net_revenue_filled = net_revenue_pivot.fillna(0).melt(id_vars='Customer Name', var_name='Year',
                                                          value_name='Net Revenue')

    # creation of sheet
    sheet = gross_revenue_filled.merge(net_revenue_filled, on=['Customer Name', 'Year'], how='left')

    # create gross margin
    sheet['Revenue Margin'] = sheet['Net Revenue'] / sheet['Revenue']

    # assign cohort to customer on sheet
    sheet = sheet.merge(cohort_assigner, on='Customer Name', how='left')

    # assign segment to customer gross revenue on sheet
    sheet['Segment'] = sheet['Revenue'].apply(assign_segment)

    # create new gross vars (last years gross rev, next years gross rev, and change from last year gross rev)
    sheet['Rev_Last_Year'] = sheet.sort_values(by='Year', ascending=True).groupby('Customer Name')['Revenue'].shift(1)
    sheet['Rev_Next_Year'] = sheet.sort_values(by='Year', ascending=True).groupby('Customer Name')['Revenue'].shift(-1)
    sheet['Rev_Change_YoY'] = sheet['Revenue'] - sheet['Rev_Last_Year']

    # create new net vars (last years net rev, next years net rev, and change from last year net rev)
    sheet['Net_Rev_Last_Year'] = sheet.sort_values(by='Year', ascending=True).groupby('Customer Name')[
        'Net Revenue'].shift(1)
    sheet['Net_Rev_Next_Year'] = sheet.sort_values(by='Year', ascending=True).groupby('Customer Name')[
        'Net Revenue'].shift(-1)
    sheet['Net_Rev_Change_YoY'] = sheet['Net Revenue'] - sheet['Net_Rev_Last_Year']
    return sheet.pivot(index='Customer Name', columns='Year', values='Revenue Margin').reset_index()


def get_other_revenue_pivot(data, column):
    other_rev_category = pd.DataFrame(data.groupby(by=[column, 'Year'])['Revenue'].sum()).reset_index()

    yearly_other_rev = data.groupby('Year')['Revenue'].sum().reset_index()

    # merge yearly spend to each company each year's other rev
    other_rev_category = other_rev_category.merge(yearly_other_rev, on='Year', how='left')

    other_rev_category = other_rev_category.rename(
        columns={"Revenue_x": "Revenue", "Revenue_y": "Yearly Revenue"})
    other_rev_category['% of Total'] = other_rev_category["Revenue"] / other_rev_category["Yearly Revenue"] * 100
    return other_rev_category.pivot(index=column, columns='Year', values='% of Total').reset_index()


ANALYSES_MAP = {
    'Revenue': get_revenue_pivot,
    'Net Revenue': get_net_revenue_pivot,
    'Revenue Margin': get_revenue_margin_pivot,
    'Other Revenue': get_other_revenue_pivot
}

import gc
import traceback

from kivy.app import App
import utils.config_kivy
from kivy.input.providers.mouse import MouseMotionEvent

from kivy.base import ExceptionHandler, ExceptionManager
from kivy.uix.screenmanager import SlideTransition, NoTransition
import widgets.factory_reg
import utils.logger as logger
from kivymd.theming import ThemeManager
from screens.screen_manager import sm, screens
from settings import INIT_SCREEN, WIDTH, HEIGHT

# Needed to remove red circles from right/mid mouse buttons clicks
from widgets.dialog import InfoDialog

MouseMotionEvent.update_graphics = lambda *args: None


class BHExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(BHExceptionHandler())


class DataDanApp(App):

    current_screen = None
    exception = None
    theme_cls = ThemeManager()

    def build(self):
        self.title = 'DataDan'
        self.switch_screen(INIT_SCREEN)
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        """
        Switch to the other screen
        :param screen_name: destination screen name
        :param direction: direction of transition
        :param duration: transition time
        :return:
        """
        if sm.has_screen(screen_name):                  # Already in the destination screen?
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)

            logger.info(f'=== Switched to {screen_name} screen')

            # Delete the old screen
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    @staticmethod
    def show_hint_popup(x, y, content):
        popup = InfoDialog()
        popup.text = content
        x = x if (x < WIDTH - len(content) * 7) else (x - len(content) * 7)
        y = (y - 15) if y > 20 else (y + 15)
        popup.pos_hint = {'center_x': (x + len(content) * 4) / WIDTH, 'center_y': y / HEIGHT}
        popup.open()

    def save_exception(self, ex):
        """
        This function is called by the Exception Handler.
        :param ex:
        :return:
        """
        self.exception = ex

    def get_exception(self):
        return self.exception


if __name__ == '__main__':

    app = DataDanApp()

    app.run()

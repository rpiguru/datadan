from kivy.lang import Builder
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.floatlayout import FloatLayout
from kivy.vector import Vector

from assets.styles import defaultstyle
from kivy.properties import OptionProperty, StringProperty, ListProperty
from kivy.uix.button import Button


Builder.load_file('widgets/kv/button.kv')


class DDButton(Button):
    button_type = OptionProperty('white', options=['blue', 'white'])

    def __init__(self, **kwargs):
        super(DDButton, self).__init__(**kwargs)
        self.on_button_type()

    def set_white_color(self):
        self.color = defaultstyle.COLOR_1
        self.background_normal = 'assets/images/buttons/white_button.png'
        self.background_down = 'assets/images/buttons/white_button_pressed.png'

    def set_default_color(self):
        self.color = (1, 1, 1, 1)
        self.background_normal = 'assets/images/buttons/blue_button.png'
        self.background_down = 'assets/images/buttons/blue_button_pressed.png'

    def on_button_type(self, *args):
        if self.button_type == 'blue':
            self.set_default_color()
        else:
            self.set_white_color()


class CircularButton(ButtonBehavior, FloatLayout):

    text = StringProperty('')
    text_color = ListProperty([.1, .1, .1, 1])
    background_color = ListProperty([0.13, 0.59, 0.95, 1])

    def collide_point(self, x, y):
        return Vector(x, y).distance(self.center) <= self.width / 2

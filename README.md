# DataDan

Data Processing Application.

## Installation

### Install Kivy Framework

- OS X
    
    ```bash
    python -m pip install kivy
    ```

- Windows

    ```bash
    python -m pip install -U pip wheel setuptools
    python -m pip install docutils pygments pypiwin32 kivy_deps.sdl2==0.1.* kivy_deps.glew==0.1.*
    python -m pip install kivy_deps.gstreamer==0.1.*
    python -m pip install kivy==1.11.1
    ```

### Install dependencies

```bash
python -m pip install -r requirements.txt
```

## Launch application

```bash
python main.py
```

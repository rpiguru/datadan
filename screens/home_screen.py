import os
import threading
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty

from screens.base import BaseScreen
from settings import DEBUG, TEST_FILE
from utils import logger
from utils.data_processor import read_raw_data, COLUMN_SIZE_HINTS, ANALYSES_MAP, filter_data_by_columns
from widgets.button import DDButton
from widgets.dataview.widgets import DataViewWidget
from widgets.dialog import FileChooserDialog, LoadingDialog, DataViewDialog, FilterDialog, OtherFilterDialog, \
    ColumnMapDialog

Builder.load_file('screens/home_screen.kv')


class HomeScreen(BaseScreen):

    source_file = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._file_chooser_dlg = None
        self.loading_dlg = LoadingDialog()
        self._reset_variables()

    def _reset_variables(self):
        self.raw_data = {}
        self.unique_data = {}
        self._column_mapped = {}
        self._cur_data = None
        self._filtered_data = None
        self.filters = {}

    def on_enter(self, *args):
        super(HomeScreen, self).on_enter(*args)
        self.ids.spin_sheet.bind(on_changed=lambda w: self._render_sheet())

    def on_btn_open(self):
        self._reset_variables()
        if not DEBUG:
            if self._file_chooser_dlg is None:
                self._file_chooser_dlg = FileChooserDialog(mode='file', extensions=['xlsx', ])
                self._file_chooser_dlg.bind(on_confirm=self._on_file_selected)
                self._file_chooser_dlg.bind(on_dismiss=lambda d: self._on_file_chooser_closed())
                self._file_chooser_dlg.open()
        else:
            self._on_file_selected(*(None, TEST_FILE, ))

    def _on_file_selected(self, *args):
        logger.info(f"Selected source file - {args[1]}")
        self.source_file = args[1]
        self._file_chooser_dlg = None
        self.loading_dlg.open()
        threading.Thread(target=self._parse_source_file).start()

    def _on_file_chooser_closed(self):
        self._file_chooser_dlg = None

    def _parse_source_file(self):
        self.raw_data = read_raw_data(self.source_file)
        Clock.schedule_once(lambda dt: self._render_result())

    def _render_result(self):
        self.ids.lb_source.text = self.source_file
        self.ids.box.disabled = False
        self.loading_dlg.dismiss()
        self.ids.spin_sheet.values = self.raw_data.keys()

    def open_filter_dlg(self):
        dlg = FilterDialog(filters=self.filters, data=self.unique_data[self.ids.spin_sheet.get_value()])
        dlg.bind(on_confirm=self._on_filter_dlg_confirmed)
        dlg.open()

    def _on_filter_dlg_confirmed(self, *args):
        filters = args[0].get_selected_filters()
        if self.filters != filters:
            logger.info(f"New filters: {filters}")
            self.filters = filters
            self._filtered_data = filter_data_by_columns(self._cur_data, self.filters)

    def _render_sheet(self):
        cur_sheet = self.ids.spin_sheet.get_value()
        if not self._column_mapped.get(cur_sheet):
            dlg = ColumnMapDialog(columns=self.raw_data[cur_sheet].columns)
            dlg.bind(on_confirm=self._on_column_names_selected)
            dlg.open()
        else:
            self._on_column_names_selected()

    def _on_column_names_selected(self, *args):
        cur_sheet = self.ids.spin_sheet.get_value()
        if args:
            column_map = args[0].get_column_map()
            self._column_mapped[self.ids.spin_sheet.get_value()] = True
            self.raw_data[cur_sheet] = self.raw_data[cur_sheet].rename(columns=column_map).fillna('')
            self.unique_data[cur_sheet] = {col: self.raw_data[cur_sheet][col].unique() for col in column_map.values()}

        self._cur_data = self.raw_data[cur_sheet]
        headers = self._cur_data.columns
        self.ids.box_heading.clear_widgets()
        for head in headers:
            btn = DDButton(text=str(head), size_hint_x=COLUMN_SIZE_HINTS.get(head, 1), height=35, button_type='blue')
            self.ids.box_heading.add_widget(btn)
        data = []
        for i, row in self._cur_data.iterrows():
            for head in headers:
                data.append({
                    'Index': i,
                    'text': row[head] if type(row[head]) == str else str(round(row[head], 5)),
                    'size_hint_x': COLUMN_SIZE_HINTS.get(head, 1),
                    'height': 30,
                    'selectable': False})
        self.ids.box_content.clear_widgets()
        self.ids.box_content.add_widget(DataViewWidget(data=data, cols=len(headers)))
        self._filtered_data = filter_data_by_columns(self._cur_data, self.filters)
        self.open_filter_dlg()

    def on_btn_other_analysis(self, analyses):
        dlg = OtherFilterDialog(data=self.unique_data[self.ids.spin_sheet.get_value()])
        dlg.bind(on_confirm=partial(self._on_other_column_selected, analyses))
        dlg.open()

    def _on_other_column_selected(self, analyses, *args):
        func = ANALYSES_MAP.get(analyses)
        if callable(func):
            column = args[0].get_selected_column()
            data = func(self._filtered_data, column)
            self._show_analyses_result(analyses, data, title=f"{analyses}(%)", round_num=2)

    def on_btn_analysis(self, analyses):
        func = ANALYSES_MAP.get(analyses)
        if callable(func):
            data = self._cur_data if analyses == 'Revenue' else self._filtered_data
            data = func(data, self.filters)
            self._show_analyses_result(analyses, data, title=analyses)

    def _show_analyses_result(self, analyses, data, title="", round_num=1):
        sheet_name = self.ids.spin_sheet.get_value()
        f_name = f'{os.path.splitext(self.source_file)[0]}_{sheet_name}_{analyses.replace(" ", "_")}_Pivot.csv'
        file_path = os.path.join(os.path.dirname(self.source_file), f_name)
        DataViewDialog(title=title, data=data, file_path=file_path, round_num=round_num).open()

    def on_btn_info(self, widget, content=""):
        widget_x, widget_y = self.to_window(widget.x, widget.y)
        self.app.show_hint_popup(widget_x, widget_y, content)

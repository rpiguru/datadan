import datetime
from kivy.logger import Logger


def debug(msg):
    Logger.debug(f'{datetime.datetime.now()}: {msg}')


def info(msg):
    Logger.info(f'{datetime.datetime.now()}: {msg}')


def warning(msg):
    Logger.warning(f'{datetime.datetime.now()}: {msg}')


def error(msg):
    Logger.error(f'{datetime.datetime.now()}: {msg}')


def exception(e):
    Logger.exception(f'{datetime.datetime.now()}: {e}')

from kivy.factory import Factory

from kivymd.button import MDRaisedButton, MDFlatButton, MDIconButton
from kivymd.card import MDCard, MDSeparator
from kivymd.progressbar import MDProgressBar
from kivymd.selectioncontrols import MDCheckbox
from kivymd.spinner import MDSpinner
from kivymd.textfields import MDTextField
from widgets.checkbox import LabeledCheckbox
from widgets.dataview.widgets import DataViewWidget
from widgets.label import ScrollableLabel, ColoredLabel
from widgets.spinner import DDSpinner

Factory.register('MDCard', cls=MDCard)
Factory.register('MDRaisedButton', cls=MDRaisedButton)
Factory.register('MDFlatButton', cls=MDFlatButton)
Factory.register('MDIconButton', cls=MDIconButton)
Factory.register('MDSpinner', cls=MDSpinner)
Factory.register('MDTextField', cls=MDTextField)
Factory.register('MDCheckbox', cls=MDCheckbox)
Factory.register('MDSeparator', cls=MDSeparator)
Factory.register('MDProgressBar', cls=MDProgressBar)
Factory.register('DDSpinner', cls=DDSpinner)
Factory.register('ScrollableLabel', cls=ScrollableLabel)
Factory.register('LabeledCheckbox', cls=LabeledCheckbox)
Factory.register('ColoredLabel', cls=ColoredLabel)
Factory.register('DataViewWidget', cls=DataViewWidget)

from kivy.uix.screenmanager import ScreenManager

from screens.error_screen import ErrorScreen
from screens.home_screen import HomeScreen


screens = {
    'home_screen': HomeScreen,
    'error_screen': ErrorScreen,
}


sm = ScreenManager()

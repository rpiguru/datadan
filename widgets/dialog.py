import os
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import OptionProperty, StringProperty, ListProperty, NumericProperty, ObjectProperty
from kivy.uix.modalview import ModalView
from kivy.uix.popup import Popup

from kivymd.dialog import MDDialog
from kivymd.theming import ThemableBehavior
from utils import logger
from utils.data_processor import COLUMN_SIZE_HINTS, DEFAULT_COLUMN_MAP
from widgets.button import DDButton
from widgets.snackbar import show_error_snackbar, show_info_snackbar
from widgets.spinner import DDSpinner

Builder.load_file('widgets/kv/dialog.kv')


class FileChooserDialog(Popup):
    mode = OptionProperty('file', options=['file', 'dir'])
    extensions = ListProperty()
    path = StringProperty('.')

    def __init__(self, **kwargs):
        super(FileChooserDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')

    def filter_content(self, directory, filename):
        if self.mode == 'file':
            if self.extensions:
                return filename.split('.')[-1] in self.extensions
            else:
                return True
        else:
            return os.path.isdir(os.path.join(directory, filename))

    def on_ok(self):
        if os.path.isfile(self.ids.txt_path.text) or self.mode == 'dir':
            self.dispatch('on_confirm', self.ids.txt_path.text)
            self.dismiss()
        else:
            show_error_snackbar(text='Please select file')

    def on_confirm(self, *args):
        pass

    def selected(self):
        self.ids.txt_path.text = str(self.ids.fc.selection[0])


class LoadingDialog(ModalView):
    pass


class ProgressDialog(ThemableBehavior, ModalView):

    title = StringProperty()
    value = NumericProperty()

    def update_progress(self, title="", value=0):
        self.title = title
        self.value = value


class MsgDialog(MDDialog):
    message = StringProperty()


class InfoDialog(ModalView):

    text = StringProperty()


class DataViewDialog(MDDialog):

    data = ObjectProperty(None, allownone=True)

    def __init__(self, file_path=None, round_num=1, **kwargs):
        super(DataViewDialog, self).__init__(**kwargs)
        self.file_path = file_path
        self.round_num = round_num

    def on_open(self, *largs):
        headers = self.data.columns
        for head in headers:
            btn = DDButton(text=str(head), size_hint_x=COLUMN_SIZE_HINTS.get(head, 1), height=35, button_type='blue')
            self.ids.box_heading.add_widget(btn)
        data = []
        for i, row in self.data.fillna('').iterrows():
            for head in headers:
                data.append({
                    'Index': i,
                    'text': row[head] if type(row[head]) == str else str(round(row[head], self.round_num)),
                    'size_hint_x': COLUMN_SIZE_HINTS.get(head, 1),
                    'height': 30,
                    'selectable': False})
        self.ids.dv.cols = len(headers)
        self.ids.dv.data = data

    def on_btn_export(self):
        self.data.to_csv(self.file_path, index=False)
        msg = f"Exported result to {self.file_path}"
        logger.info(msg)
        show_info_snackbar(msg)


class FilterDialog(MDDialog):

    def __init__(self, filters, data=None, **kwargs):
        super(FilterDialog, self).__init__(**kwargs)
        self.filters = filters
        self.data = data
        self.register_event_type('on_confirm')
        Clock.schedule_once(lambda dt: self._bind_events())

    def _bind_events(self):
        for k in self.data.keys():
            _key = '_'.join([v.lower() for v in str(k).split()])
            if f"chk_{_key}" in self.ids:
                self.ids[f"chk_{_key}"].bind(on_changed=partial(self._on_chk_changed, _key, k))
                if k in self.filters:
                    self.ids[f"chk_{_key}"].active = True
                    self.ids[f"spin_{_key}"].set_value(self.filters[k])

    def _on_chk_changed(self, _key, column, *args):
        if self.ids[f"chk_{_key}"].active:
            self.ids[f"spin_{_key}"].disabled = False
            self.ids[f"spin_{_key}"].values = [str(val) for val in self.data[column]]
        else:
            self.ids[f"spin_{_key}"].values = []
            self.ids[f"spin_{_key}"].disabled = True

    def on_open(self):
        for k in self.filters.keys():
            _key = '_'.join([v.lower() for v in str(k).split()])
            self.ids[f"chk_{_key}"].active = True

    def get_selected_filters(self):
        filters = {}
        for k in self.data.keys():
            _key = '_'.join([v.lower() for v in str(k).split()])
            if f"chk_{_key}" in self.ids and self.ids[f"chk_{_key}"].active:
                filters[k] = self.ids[f"spin_{_key}"].get_value()
        return filters

    def on_btn_ok(self):
        self.dispatch('on_confirm')
        self.dismiss()

    def on_confirm(self, *args):
        pass


class OtherFilterDialog(MDDialog):

    def __init__(self, data=None, **kwargs):
        super(OtherFilterDialog, self).__init__(**kwargs)
        self.data = data
        self.register_event_type('on_confirm')

    def on_open(self):
        # self.ids.spin_column.bind(on_changed=self._on_column_changed)
        self.ids.spin_column.values = [k for k in self.data.keys() if k != "Revenue"]

    # def _on_column_changed(self, *args):
    #     self.ids.spin_value.values = [str(v) for v in self.data[args[0].get_value()] if v]

    def get_selected_column(self):
        return self.ids.spin_column.get_value()

    def on_btn_ok(self):
        self.dispatch('on_confirm')
        self.dismiss()

    def on_confirm(self, *args):
        pass


class ColumnMapDialog(MDDialog):

    def __init__(self, columns=None, **kwargs):
        super(ColumnMapDialog, self).__init__(**kwargs)
        self.columns = columns
        self.register_event_type('on_confirm')

    def on_open(self):
        for wid in self.ids.box.children:
            if isinstance(wid, DDSpinner):
                wid.values = self.columns
                default_val = [k for k, v in DEFAULT_COLUMN_MAP.items() if v == wid.key][0]
                if default_val in self.columns:
                    wid.set_value(default_val)

    def on_btn_ok(self):
        columns = [w.get_value() for w in self.ids.box.children if isinstance(w, DDSpinner)]
        if len(columns) != len(set(columns)):
            show_error_snackbar('Column names cannot be duplicated!')
            return
        else:
            self.dispatch('on_confirm')
            self.dismiss()

    def get_column_map(self):
        col_map = {}
        for w in self.ids.box.children:
            if isinstance(w, DDSpinner):
                col_map[w.get_value()] = w.key
        return col_map

    def on_confirm(self, *args):
        pass

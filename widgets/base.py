from abc import abstractmethod

from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout


class DefaultInput:
    """
    If you want to create any input widget you should inherit this class.
    For example::

        MyAwesomeInput(TextInput, DefaultInput):

    Also make sure your widget doesn't contain any other widgets with the key property
    otherwise the screens.base.screen.BaseScreen will collect them too.
    """

    key = StringProperty(None)
    """Key used to collect data from screen
    """

    required = BooleanProperty(True)

    error_label_id = StringProperty('error')
    """Label id to display error messages
    """

    error_label_height = NumericProperty(15)
    """Height of error Label on error showing
    """

    error_label_width = NumericProperty()
    """Width of an error Label"""

    child_input = StringProperty(None)
    """Id of the child input widget
    If you want to create an input widget like::
        BoxLayout:
            Label:
                text: 'Input Name'

            TextInput:
                id: input

    just set this property to TextInout id.
    Works if there is only one child input
    """

    @property
    def _widget(self):
        if self.child_input:
            return self.ids[self.child_input]
        else:
            return self

    def show_error(self, message):
        self.mark_as_error()
        self.show_error_message(message)

    def clear_errors(self):
        self.mark_as_normal()
        self.hide_error_message()

    def is_empty(self):
        if self.child_input:
            return self._widget.is_empty()
        else:
            value = self._get_value()
            return value in ['', None, []]  # checkbox can return False

    def _validate(self):
        errors = []
        if self.is_empty() and self.required:
            errors.append('This field may not be blank')

        custom_errors = self.custom_error_check()
        if custom_errors:
            errors += custom_errors

        return errors

    def validate(self):
        """
        If there is any errors returns a dict of field key and errors list
        similar to server response
        :return:
        """
        errors = self._validate()
        return {self.key: errors} if errors else {}

    def _get_error_message_label(self):
        if hasattr(self.ids, self.error_label_id):
            return getattr(self.ids, self.error_label_id)

    def show_error_message(self, message):
        """
        Override to put the error message to specific label on your widget
        :param message: str
        :return:
        """
        widget = self._get_error_message_label()
        if widget:
            widget.text = message
            widget.height = self.error_label_height
            if self.error_label_width:
                widget.size_hint_x = None
                widget.width = self.error_label_width

    def hide_error_message(self):
        widget = self._get_error_message_label()
        if widget:
            widget.text = ''
            widget.height = 0.1

    def get_value(self):
        """
        Returns a dict of key and value
        :return: dict()
        """
        return {self.key: self._widget._get_value()}

    @abstractmethod
    def custom_error_check(self):
        """
        Write your custom error check here
        :return: list
        """
        if self.child_input:
            return self._widget.custom_error_check()

    @abstractmethod
    def _get_value(self):
        """
        Returns value
        :return: any
        """
        if self.child_input:
            return self._widget._get_value()

    @abstractmethod
    def set_value(self, value):
        """
        Set the widget value
        :param value:
        :return:
        """
        if self.child_input:
            self._widget.set_value(value)

    @abstractmethod
    def mark_as_error(self):
        """
        Highlight your widget
        :return:
        """
        if self.child_input:
            self._widget.mark_as_error()

    @abstractmethod
    def mark_as_normal(self):
        """
        Remove highlight
        :return:
        """
        if self.child_input:
            self._widget.mark_as_normal()


class LabeledInputBase(BoxLayout, DefaultInput):
    _optional_text = StringProperty()
    """Hack for localization
    """

    text = StringProperty()

    child_input = 'input'
    input_height = NumericProperty(50)

    error_font_size = NumericProperty(14)
    """Font side used for error label
    """

    error_font_bold = BooleanProperty(False)
    """Set it to use bold font for error messages
    """

    label_color = ListProperty()

    def __init__(self, **kwargs):
        super(LabeledInputBase, self).__init__(**kwargs)
        self.label_color = [0, 0, 0, 1]
        self.bind(text=self._update_text)
        self.bind(required=self._update_text)
        self.bind(_optional_text=self._update_text)

    def _update_text(self, *args):
        if not self.required and len(self._optional_text.strip()) > 0:  # skip if `optional_text == ''
            self.ids.text.text = '{} [color=78909c]({})'.format(self.text, self._optional_text)
        else:
            self.ids.text.text = self.text

    def set_value(self, value):
        self.ids.input.set_value(value)

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.clear_errors()

        super(LabeledInputBase, self).on_touch_down(touch)

    def mark_as_normal(self):
        pass

    def mark_as_error(self):
        pass

    def _get_value(self):
        pass

    def custom_error_check(self):
        pass
